package com.example.purificadora;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.purificadora.utilities.Request;

public class login extends AppCompatActivity {
    TextView Registrarse;
    ImageButton VerContrasena;
    EditText Usuario;
    EditText Contrasena;
    Button entrar;
    Context contextClass;
    public static String usuariolog,passwordlog;

    Request request = new Request();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        Registrarse = (TextView)findViewById(R.id.registrarsetxt);
        VerContrasena = (ImageButton) findViewById(R.id.ViewPass);
        Usuario = (EditText)findViewById(R.id.usuariotxt);
        Contrasena = (EditText)findViewById(R.id.passwordtxt);
        entrar = (Button)findViewById(R.id.entrarbtn);

        VerContrasena.setBackgroundDrawable(null);

        compruebaInternet("No cuenta con conexión a internet, verifique su conexión");

        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Usuario.length() == 0){
                    Toast validacionUsuario = Toast.makeText(login.this,"Ingrese el usuario",Toast.LENGTH_SHORT);
                    validacionUsuario.show();
                }else if (Contrasena.length() == 0){
                    Toast validacionContrasena = Toast.makeText(login.this,"Ingrese la contraseña",Toast.LENGTH_SHORT);
                    validacionContrasena.show();
                }else{
                    usuariolog = Usuario.getText().toString();
                    passwordlog = Contrasena.getText().toString();
                    request.getClienteLogin(login.this,usuariolog,passwordlog);

                }
            }
        });


        Registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(login.this,Registro.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });


      VerContrasena.setOnTouchListener(new View.OnTouchListener() {
          @Override
          public boolean onTouch(View view, MotionEvent motionEvent) {
              switch (motionEvent.getAction()){
                  case MotionEvent.ACTION_DOWN:
                      Contrasena.setInputType(InputType.TYPE_CLASS_TEXT);
                      Contrasena.setSelection(Contrasena.getText().length());
                      break;
                  case MotionEvent.ACTION_UP:
                      Contrasena.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                      Contrasena.setSelection(Contrasena.getText().length());
              }
              return false;
          }
      });




    }

    @Override
    protected void onResume() {
        super.onResume();
        compruebaInternet("Verifique su conexión e intente nuevamente");
    }

    @Override
    public void onBackPressed() {

        dialogoSalida(this);
    }

    private void dialogoSalida(Activity activity) {
        new AlertDialog.Builder(activity,R.style.CustomDialogTheme)
                .setCancelable(false)
                .setTitle("ADVERTENCIA")
                .setMessage("¿Desea salir de la aplicación?")
                .setPositiveButton("ACEPTAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();


    }

    public boolean compruebaInternet (String mensaje){

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
           return  true;
        } else {
            Toast validacionInternet = Toast.makeText(login.this,mensaje,Toast.LENGTH_SHORT);
            validacionInternet.show();
            return  false;

        }
    }

}
