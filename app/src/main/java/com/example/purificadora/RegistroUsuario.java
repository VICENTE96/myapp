package com.example.purificadora;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.purificadora.utilities.Request;

import static com.example.purificadora.Registro.calle_numero;
import static com.example.purificadora.Registro.colonia;
import static com.example.purificadora.Registro.nombre;
import static com.example.purificadora.Registro.referencias;
import static com.example.purificadora.Registro.telefono;

public class RegistroUsuario extends AppCompatActivity {
    EditText Usuario,Contrasena,ConfirmarContrasena;
    Button Finalizar;
    public static String pass,confPass,user;

    Request request = new Request();
    Registro registro = new Registro();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro_usuario);
        Usuario = (EditText)findViewById(R.id.NombreUsuario);
        Contrasena = (EditText)findViewById(R.id.Contrasena);
        ConfirmarContrasena = (EditText)findViewById(R.id.ConfContrasena);
        Finalizar = (Button)findViewById(R.id.FinalizarBtn);


        Finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                char x;
                 boolean validaUser = false;
                user = Usuario.getText().toString();
                pass = Contrasena.getText().toString();
                confPass = ConfirmarContrasena.getText().toString();

                for (int i = 0; i <user.length() ; i++) {
                    x = user.charAt(i);
                    if(x == ' '){
                        validaUser = true;
                    }
                }

                if(Usuario.length()== 0){
                    Toast usuarioVacio = Toast.makeText(RegistroUsuario.this,"Ingrese un nombre de usuario",Toast.LENGTH_LONG);
                    usuarioVacio.show();
                }
                else if (Contrasena.length() == 0) {
                    Toast contrasenaVacia = Toast.makeText(RegistroUsuario.this,"Escriba su contraseña",Toast.LENGTH_LONG);
                    contrasenaVacia.show();
                }
                else if (ConfirmarContrasena.length() == 0){
                    Toast confCont = Toast.makeText(RegistroUsuario.this,"Confirme su contraseña",Toast.LENGTH_LONG);
                    confCont.show();
                }else{
                    if (!pass.equals(confPass)){
                        Toast contrasenaIgual = Toast.makeText(RegistroUsuario.this,"Las contraseñas no cohinciden, favor de revisar",Toast.LENGTH_LONG);
                        contrasenaIgual.show();
                    }else{

                        if(validaUser == true){
                            Toast validauser = Toast.makeText(RegistroUsuario.this,"Ingresa un nombre de usuario sin espacios",Toast.LENGTH_LONG);
                            validauser.show();
                        }
                        else if (Usuario.length() > 20) {
                            Toast contrasenaVacia = Toast.makeText(RegistroUsuario.this,"Esctiba un nombre de usuario no mayor a 20 letras",Toast.LENGTH_LONG);
                            contrasenaVacia.show();
                        }else{
                            dialogoEnviar(RegistroUsuario.this);
                        }
                    }
                }


            }
        });

    }
    private void dialogoEnviar(Activity activity) {
        new AlertDialog.Builder(activity,R.style.CustomDialogTheme)
                .setCancelable(false)
                .setTitle("CONFIRMAR")

                .setMessage("Su nombre de usuario será\n\' "  + user + " \'\ncon este nombre  podrá iniciar sesión" )
                .setPositiveButton("ACEPTAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                request.sendNewCliente(RegistroUsuario.this,nombre,telefono,colonia,calle_numero,referencias);
                            }
                        })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
    }
}
