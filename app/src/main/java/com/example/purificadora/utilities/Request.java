package com.example.purificadora.utilities;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.purificadora.ControlUsuario;
import com.example.purificadora.login;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.purificadora.RegistroUsuario.confPass;
import static com.example.purificadora.RegistroUsuario.pass;
import static com.example.purificadora.RegistroUsuario.user;


public class Request {
    ProgressDialog progressDialog;
    long idCliente;
    int autorizacion;



    Services services = new Services();
    public void sendNewCliente(final Context context, final String nombre, Long telefono, String colonia, String calle_numero, String referencias) {
        progressDialog = new BarraCarga().showDialog(context);
        progressDialog.show();
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("nombre", nombre);
            jsonObject.put("telefono", telefono);
            jsonObject.put("colonia", colonia);
            jsonObject.put("calle_numero", calle_numero);
            jsonObject.put("referencias", referencias);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<JsonObject> call = services.RequestPost(jsonObject).sendNewCliente();

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {

                    progressDialog.dismiss();
                    getClienteNombre(context,progressDialog,nombre);

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Error al enviar información, intente nuevamente" , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, "Verifique su conexión e intente nuevamente" , Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getClienteNombre(final Context context, final ProgressDialog progressDialog, String nombre) {

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("nombre", nombre);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<JsonObject> call = services.RequestPost(jsonObject).getClienteNombre();

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    idCliente = Long.parseLong(response.body().get("d").toString());
                    progressDialog.dismiss();
                    //Toast.makeText(context, "id " + idCliente , Toast.LENGTH_LONG).show();
                    sendNewUsuario(context,progressDialog,idCliente,user,confPass,true);
                }
                else if (response.code() == 500) {
                    progressDialog.dismiss();
                    Toast.makeText(context,"error 500"  , Toast.LENGTH_LONG).show();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Error al enviar asd, intente nuevamente" , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, "Verifique su conexión e intente nuevamente" , Toast.LENGTH_LONG).show();
            }
        });
    }

    public void sendNewUsuario(final Context context,final ProgressDialog progressDialog, long id_Cliente, String nombre_usuario, String password, boolean activo) {

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id_Cliente", id_Cliente);
            jsonObject.put("nombre_Usuario", nombre_usuario);
            jsonObject.put("password", password);
            jsonObject.put("activo", activo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<JsonObject> call = services.RequestPost(jsonObject).sendNewUsuario();

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(final Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {

                    progressDialog.dismiss();
                    Toast.makeText(context, "Ha realizado el registro con exito, ahora puede iniciar sesión" , Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context, login.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    context.startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Error al enviar usuario, intente nuevamente" , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, "Verifique su conexión e intente nuevamente" , Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getClienteLogin(final Context context, String usuario, String password) {
        progressDialog = new BarraCarga().showDialog(context);
        progressDialog.show();
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user", usuario);
            jsonObject.put("pass", password);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<JsonObject> call = services.RequestPost(jsonObject).getLogin();

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                     //autorizacion = Long.parseLong(String.valueOf(response.body().get("d").getAsBigInteger()));
                    autorizacion = response.body().get("d").getAsInt();

                   if (autorizacion >= 1){
                        Intent intent = new Intent(context, ControlUsuario.class);
                        context.startActivity(intent);
                        Toast.makeText(context,"Bienvenido"  , Toast.LENGTH_LONG).show();
                    }else{
                        progressDialog.dismiss();
                        Toast.makeText(context,"Usuario ó contraseña incorrecto"  , Toast.LENGTH_LONG).show();
                    }progressDialog.dismiss();
                }
                else if (response.code() == 500) {
                    progressDialog.dismiss();
                    Toast.makeText(context,"error 500"  , Toast.LENGTH_LONG).show();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Error al enviar asd, intente nuevamente" , Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, "Verifique su conexión e intente nuevamente" , Toast.LENGTH_LONG).show();
            }
        });
    }

}
