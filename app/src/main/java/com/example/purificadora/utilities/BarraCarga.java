package com.example.purificadora.utilities;


import android.app.ProgressDialog;
import android.content.Context;

import com.example.purificadora.R;

public class BarraCarga {

    public ProgressDialog showDialog(Context context){
        ProgressDialog progressDialog = new ProgressDialog(context,R.style.MyProgressDialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Cargando...");
        return progressDialog;
    }
}
