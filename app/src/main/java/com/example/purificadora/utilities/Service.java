package com.example.purificadora.utilities;

import com.example.purificadora.Constant;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Service {

    @POST(Constant.NEW_Cliente)
    Call<JsonObject> sendNewCliente();

    @POST(Constant.GET_Cliente_NOMBRE)
    Call<JsonObject> getClienteNombre();

    @POST(Constant.NEW_USUARIO)
    Call<JsonObject> sendNewUsuario();

    @POST(Constant.GET_Cliente_Login)
    Call<JsonObject> getLogin();

}


