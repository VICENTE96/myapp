package com.example.purificadora;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.purificadora.utilities.BarraCarga;
import com.example.purificadora.utilities.Request;

public class Registro extends AppCompatActivity {
    EditText Nombre,Telefono,Colonia,CalleNumero,Referencias;
    TextView CalleNumeroTxv;
    Button Registro;



    public static long telefono;
    public static String nombre,colonia,calle_numero,referencias;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        Nombre = (EditText)findViewById(R.id.nombretxt);
        Telefono = (EditText)findViewById(R.id.telefonotxt);
        Colonia = (EditText)findViewById(R.id.coloniatxt);
        CalleNumero = (EditText)findViewById(R.id.callenumerotxt);
        Referencias = (EditText)findViewById(R.id.referenciastxt);
        Registro = (Button)findViewById(R.id.entrarbtn);

        CalleNumeroTxv = (TextView)findViewById(R.id.callenum);


        compruebaInternet(this);

        Registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(Telefono.length() == 0) {
                telefono = 0;
                }else{
                    telefono = Long.parseLong( Telefono.getText().toString());
                }

                 nombre = Nombre.getText().toString();
                 colonia = Colonia.getText().toString();
                 referencias = Referencias.getText().toString();
                 calle_numero = CalleNumero.getText().toString();

                char x;
                boolean bandNum = false;

                for (int i = 0; i <calle_numero.length() ; i++) {

                    x = calle_numero.charAt(i);
                    if(x == '0' ||x == '1' ||x == '2' ||x == '3' ||x == '4' ||x == '5' ||x == '6' ||x == '7' ||x == '8' ||x == '9' ){
                        bandNum = true;
                    }
                }


                if (Nombre.length() == 0){
                    Toast validacionNombre =  Toast.makeText(Registro.this,"Ingrese su nombre", Toast.LENGTH_SHORT);
                    validacionNombre.show();
                }else if (Telefono.length() == 0){
                    Toast validacionTelefono =  Toast.makeText(Registro.this,"Ingrese su teléfono", Toast.LENGTH_SHORT);
                    validacionTelefono.show();
                }else if (Colonia.length() == 0){
                    Toast validacionColonia =  Toast.makeText(Registro.this,"Ingrese su colonia", Toast.LENGTH_SHORT);
                    validacionColonia.show();
                }else if (CalleNumero.length() == 0){
                    Toast validacionCalleNumero =  Toast.makeText(Registro.this,"Ingrese su calle y número de domicilio", Toast.LENGTH_SHORT);
                    validacionCalleNumero.show();
                }else if (Referencias.length() == 0){
                    Toast validacionReferencia =  Toast.makeText(Registro.this,"Ingrese alguna referencia", Toast.LENGTH_SHORT);
                    validacionReferencia.show();
                }else{
                    if (bandNum == false) {
                        Toast faltaNumero = Toast.makeText(Registro.this, "Ingrese su número de domicilio ", Toast.LENGTH_SHORT);
                        faltaNumero.show();
                        CalleNumeroTxv.setTextColor(Color.parseColor("#CB4335"));
                    }else{
                        CalleNumeroTxv.setTextColor(Color.parseColor("#FFFFFF"));

                        //request.sendNewCliente(Registro.this,nombre,telefono,colonia,calle_numero,referencias);
                        Intent intent = new Intent(Registro.this, RegistroUsuario.class);
                        startActivity(intent);
                    }
                }
            }

        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        compruebaInternet(this);
    }

    public boolean compruebaInternet (Activity activity){

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return  true;
        } else {


            new AlertDialog.Builder(activity,R.style.CustomDialogTheme)
                    .setCancelable(false)
                    .setTitle("ADVERTENCIA")
                    .setMessage("Para continuar es necesario tener conexión a internet")
                    .setPositiveButton("ACEPTAR",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();

                                }
                            })
                    .setNegativeButton(" ",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
            return  false;

        }
    }
}

